package studentCoursesBackup.util;

import java.util.ArrayList;

import studentCoursesBackup.myTree.Node;

public interface FileDisplayInterface
{
	public void writeToFile(ArrayList<Node> Original_Node, String fileName);
}
