package studentCoursesBackup.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import studentCoursesBackup.myTree.Node;

public class Results implements FileDisplayInterface, StdoutDisplayInterface
{
	FileWriter write = null;

	public void printNodes(ArrayList<Node> Original_Node, String fileName)
	{
		writeToScreen(Original_Node);
		writeToFile(Original_Node, fileName);
	}

	public void writeToScreen(ArrayList<Node> Original_Node)
	{
		for (int i = 0; i < Original_Node.size(); i++)
		{
			System.out.print(Original_Node.get(i).BNumber + " -> ");
			for (int j = 0; j < Original_Node.get(i).Courses.size(); j++)

			{
				System.out.print(Original_Node.get(i).Courses.get(j) + " ");
			}
			System.out.print("\n");
		}
	}

	public void writeToFile(ArrayList<Node> Original_Node, String fileName)
	{
		int val = 1;
		try
		{
			if (val == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				val = 0;
			}

			write = new FileWriter(fileName, true);
			for (int i = 0; i < Original_Node.size(); i++)
			{
				write.write(Original_Node.get(i).BNumber + " -> ");
				for (int j = 0; j < Original_Node.get(i).Courses.size(); j++)
				{
					write.write(Original_Node.get(i).Courses.get(j) + " ");
				}
				if (i + 1 != Original_Node.size())
				{
					write.write("\n");
				}
			}
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}

	}
}
