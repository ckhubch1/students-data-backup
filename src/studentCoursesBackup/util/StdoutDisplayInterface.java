package studentCoursesBackup.util;

import java.util.ArrayList;

import studentCoursesBackup.myTree.Node;

public interface StdoutDisplayInterface
{
	public void writeToScreen(ArrayList<Node> Original_Node);
}
