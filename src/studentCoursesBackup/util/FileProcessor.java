package studentCoursesBackup.util;

import java.util.Scanner;
import java.io.File;

public class FileProcessor
{
	private Scanner x, x2;

	public void OpenFile(String fileName)
	{
		try
		{
			x = new Scanner(new File(fileName));
			x2 = new Scanner(new File(fileName));
		} catch (Exception e)
		{
			System.out.println("Unable to open file '" + fileName + "'");
		}

	}

	public int CountLines()
	{
		int count = 0;
		while (x.hasNext())
		{
			count++;
			x.nextLine();
		}
		return count;
	}

	public String readLine()
	{
		String line = null;
		line = x2.next();
		return line;
	}

	public void CloseFile()
	{
		x.close();
	}
}
