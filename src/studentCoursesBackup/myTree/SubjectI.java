package studentCoursesBackup.myTree;

public interface SubjectI
{
	public void registerObserver(Node SubjNode);

	public void removeObserver(Node node);

	public void notifyObservers(Node node);
}
